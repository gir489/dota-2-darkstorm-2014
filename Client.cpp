#include "SDK.h"
#include "Client.h"

CreateMoveFn OriginalCreateMove;

//============================================================================================
void __stdcall Hooked_CreateMove( int sequence_number, float input_sample_frametime, bool active )
{
	try
	{
		CBaseEntity* pBaseEntity = GetBaseEntity(me); //Grab the local player's entity pointer.
	
		if( pBaseEntity == NULL ) //This should never happen, but never say never. 0xC0000005 is no laughing matter.
			return;
	
		OriginalCreateMove( sequence_number, input_sample_frametime, active ); //Call the original.
	
		CUserCmd *pCommand = g_pInput->GetUserCmd(sequence_number); //Grab the CUserCmd from CInput::GetUserCmd.
	
		//Do your client hook stuff here. This function is called once per tick. For time-critical functions, run your code in PaintTraverse. For move specific functions, run them here.
	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed Hooked_CreateMove");
	}
}
//============================================================================================