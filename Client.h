#pragma once

#include "SDK.h"

typedef void* ( __stdcall* CreateMoveFn )( int, float, bool );
bool __stdcall Hooked_WriteUserCmdDeltaToBuffer( DWORD* buf, int from, int to, bool isnewcommand );
void __stdcall Hooked_CreateMove( int sequence_number, float input_sample_frametime, bool active );
void GetClassIDs ( );