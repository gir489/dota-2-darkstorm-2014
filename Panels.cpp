#include "SDK.h"
#include "Panels.h"
#include "CDrawManager.h"
#include "VMT Hook.h"

PaintTraverseFn OriginalPaintTraverse;
CScreenSize gScreenSize;
//===================================================================================
void __stdcall Hooked_PaintTraverse( int unknown, unsigned int vguiPanel, bool forceRepaint, bool allowForce)
{
	try
	{
		OriginalPaintTraverse(unknown, vguiPanel, forceRepaint, allowForce);

		static unsigned int vguiMatSystemTopPanel;

		if (vguiMatSystemTopPanel == NULL)
		{
			//gBaseAPI.LogToFile("Finding MatSystemTopPanel");
			const char* szName = g_pIPanel->GetName(vguiPanel);
			if( szName[0] == 'M' && szName[3] == 'S' ) //Look for MatSystemTopPanel without using slow operations like strcmp or strstr.
			{
				//gBaseAPI.LogToFile("Found");
				vguiMatSystemTopPanel = vguiPanel;
				Intro();
			}
		}
		
		if ( vguiMatSystemTopPanel == vguiPanel ) //If we're on MatSystemTopPanel, call our drawing code.
		{
			if( g_pEngine->IsDrawingLoadingImage() || !g_pEngine->IsInGame( ) || !g_pEngine->IsConnected() || g_pEngine->Con_IsVisible( ) || ( ( GetAsyncKeyState(VK_F12) || g_pEngine->IsTakingScreenshot( ) ) ) )
			{
				return; //We don't want to draw at the menu.
			}
			else if (gScreenSize.iScreenHeight == 0)
			{
				g_pEngine->GetScreenSize(gScreenSize.iScreenWidth, gScreenSize.iScreenHeight);
			}

			//This section will be called when the player is not at the menu game and can see the screen or not taking a screenshot.
			//gBaseAPI.LogToFile("Drawing HelloWorld");
			gDrawManager.DrawString(gScreenSize.iScreenWidth / 2 - (gDrawManager.GetPixelTextSize("Welcome to Darkstorm") / 2), 200, gDrawManager.dwGetTeamColor(3), "Welcome to Darkstorm"); //Remove this if you want.

			//Test ESP code.

			//gBaseAPI.LogToFile("Finding Local Player");
			CBaseEntity* pBaseLocalEnt;
			if (me != NULL )
			{
				pBaseLocalEnt = g_pEntList->GetClientEntity(me);  //Grab the local player's entity.
			}

			if (pBaseLocalEnt == NULL) //Always check for null pointers.
				return;
			
			for (int i = 1; i < g_pEntList->GetHighestEntityIndex(); i++)
			{
				CBaseEntity *pBaseEntity = GetBaseEntity(i);

				if (pBaseEntity == NULL)
					continue;

				char* chClientCLass = pBaseEntity->GetClientClass()->chName;

				//gBaseAPI.LogToFile(pBaseEntity->GetClientClass()->chName);
				//if (!strcmp("CDOTAPlayer", pBaseEntity->GetClientClass()->chName))
				//{
				if (chClientCLass[0] == 'C' && chClientCLass[6] == 'U' && chClientCLass[11] == 'H')
				{
					if (pBaseEntity->IsDormant() || !pBaseEntity->IsAlive())
						continue;

					if (pBaseEntity->IsIllusion())
					{
						Vector vecScreen; //Setup the Vectors.
						//pBaseEntity->GetWorldSpaceCenter(vecCenter);
						if (gDrawManager.WorldToScreen(pBaseEntity->GetAbsOrigin(), vecScreen)) //If the player is visble.
						{
							gDrawManager.DrawString(vecScreen.x - 100, vecScreen.y - 50, 0xFFFFFFFF, "Illusion"); //Draw on the player.
						}
					}
					else
					{
						Vector vecScreen; //Setup the Vectors.
						//pBaseEntity->GetWorldSpaceCenter(vecCenter);
						if (gDrawManager.WorldToScreen(pBaseEntity->GetAbsOrigin(), vecScreen)) //If the player is visble.
						{

							gDrawManager.DrawString(vecScreen.x - 100, vecScreen.y - 50, 0xFFFFFFFF, "Health: %i Mana: %f", pBaseEntity->GetHealth(), pBaseEntity->GetMana()); //Draw on the player.
						}
					}
				}
				/*else
				{
					Vector vecScreen; //Setup the Vectors.
					if (gDrawManager.WorldToScreen(pBaseEntity->GetAbsOrigin(), vecScreen)) //If the player is visble.
					{
						gDrawManager.DrawString(vecScreen.x - 50, vecScreen.y - 50, 0xFFFFFFFF, pBaseEntity->GetClientClass()->chName); //Draw on the player.
					}
				}*/
			}
		}
	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed PaintTraverse");
		gBaseAPI.ErrorBox("Failed PaintTraverse");
	}
}
//===================================================================================
void Intro( )
{
	try
	{
		gDrawManager.Initialize(); //Initalize the drawing class.

		//DWORD dwPlayerResources = gBaseAPI.GetClientSignature("8B 0D ? ? ? ? FF 30 8D 89"); //You have taken. The Tumble.
		//g_pPlayerResources = *(IGameResources**)(dwPlayerResources+2);
		//XASSERT(g_pPlayerResources);
		////#ifdef DEBUG
		//gBaseAPI.LogToFile("g_pPlayerResources client.dll+0x%X", (DWORD)g_pPlayerResources - (DWORD)gBaseAPI.GetModuleHandleSafe("client.dll"));
		////#endif
#ifdef DEBUG
		gBaseAPI.LogToFile("Injection Successful"); //If the module got here without crashing, it is good day.
#endif
	}
	catch(...)
	{
		gBaseAPI.LogToFile("Failed Intro");
		gBaseAPI.ErrorBox("Failed Intro");
	}
}