#include "CDrawManager.h"
//===================================================================================
CDrawManager gDrawManager;

#define ESP_HEIGHT 14
//===================================================================================
void CDrawManager::Initialize( )
{
	if ( g_pSurface == NULL )
		return;

#ifdef DEBUG
	gBaseAPI.LogToFile("Width: %i Height: %i", gScreenSize.iScreenWidth, gScreenSize.iScreenHeight);
#endif
	m_Font = g_pSurface->CreateFont( );
	g_pSurface->SetFontGlyphSet( m_Font, "Tahoma", ESP_HEIGHT, 500, 0, 0, 0x200 );
}
//===================================================================================
void CDrawManager::DrawString( int x, int y, DWORD dwColor, const wchar_t *pszText)
{
	if( pszText == NULL )
		return;

	g_pSurface->DrawSetTextPos( x, y );
	g_pSurface->DrawSetTextFont( m_Font );
	g_pSurface->DrawSetTextColor( RED(dwColor), GREEN(dwColor), BLUE(dwColor), ALPHA(dwColor) );
	g_pSurface->DrawPrintText( pszText, wcslen( pszText ) );
}
//===================================================================================
void CDrawManager::DrawString( int x, int y, DWORD dwColor, const char *pszText, ... )
{
	if( pszText == NULL )
		return;

	va_list va_alist;
	char szBuffer[1024] = { '\0' };
	wchar_t szString[1024] = { '\0' };

	va_start( va_alist, pszText );
	vsprintf_s( szBuffer, pszText, va_alist );
	va_end( va_alist );

	wsprintfW( szString, L"%S", szBuffer );

	g_pSurface->DrawSetTextPos( x, y );
	g_pSurface->DrawSetTextFont( m_Font );
	g_pSurface->DrawSetTextColor( RED(dwColor), GREEN(dwColor), BLUE(dwColor), ALPHA(dwColor) );
	g_pSurface->DrawPrintText( szString, wcslen( szString ) );
}
//===================================================================================
byte CDrawManager::GetESPHeight( )
{
	return ESP_HEIGHT;
}
//===================================================================================
void CDrawManager::DrawRect( int x, int y, int w, int h, DWORD dwColor )
{
	g_pSurface->DrawSetColor( RED(dwColor), GREEN(dwColor), BLUE(dwColor), ALPHA(dwColor) );
	g_pSurface->DrawFilledRect( x, y, x + w, y + h );
}
//===================================================================================
void CDrawManager::OutlineRect( int x, int y, int w, int h, DWORD dwColor )
{
	g_pSurface->DrawSetColor( RED(dwColor), GREEN(dwColor), BLUE(dwColor), ALPHA(dwColor) );
	g_pSurface->DrawOutlinedRect( x, y, x + w, y + h );
}
//===================================================================================
int CDrawManager::GetPixelTextSize(const char *pszText)
{
	if (pszText == NULL)
		return NULL;

	wchar_t szString[1024] = { '\0' };

	wsprintfW(szString, L"%S", pszText);

	int iWidth, iHeight;

	g_pSurface->GetTextSize(m_Font, szString, iWidth, iHeight);

	return iWidth;
}
//===================================================================================
int CDrawManager::GetPixelTextSize(wchar_t *pszText)
{
	if (pszText == NULL)
		return NULL;

	int iWidth, iHeight;

	g_pSurface->GetTextSize(m_Font, pszText, iWidth, iHeight);

	return iWidth;
}
//===================================================================================
void CDrawManager::DrawBox( Vector vOrigin, int r, int g, int b, int alpha, int box_width, int radius )
{
	Vector vScreen;

	if( !WorldToScreen( vOrigin, vScreen ) )
		return;

	int radius2 = radius<<1;

	OutlineRect( vScreen.x - radius + box_width, vScreen.y - radius + box_width, radius2 - box_width, radius2 - box_width, 0x000000FF );
	OutlineRect( vScreen.x - radius - 1, vScreen.y - radius - 1, radius2 + ( box_width + 2 ), radius2 + ( box_width + 2 ), 0x000000FF );
	DrawRect( vScreen.x - radius + box_width, vScreen.y - radius, radius2 - box_width, box_width,COLORCODE( r, g, b, alpha ));
	DrawRect( vScreen.x - radius, vScreen.y + radius, radius2, box_width,COLORCODE( r, g, b, alpha ));
	DrawRect( vScreen.x - radius, vScreen.y - radius, box_width, radius2,COLORCODE( r, g, b, alpha ));
	DrawRect( vScreen.x + radius, vScreen.y - radius, box_width, radius2 + box_width, COLORCODE( r, g, b, alpha ) );
}
//===================================================================================
bool CDrawManager::WorldToScreen( Vector &vOrigin, Vector &vScreen )
{
	if (gScreenSize.iScreenWidth == 0 || gScreenSize.iScreenHeight == 0)
	{
		return false;
	}

	//gBaseAPI.LogToFile("Trying to call WorldToScreenMatrix");

	const matrix3x4& worldToScreen = g_pEngine->WorldToScreenMatrix(); //Grab the world to screen matrix from CEngineClient::WorldToScreenMatrix

	//gBaseAPI.LogToFile("Got WorldToScreenMatrix");

	float w = worldToScreen[3][0] * vOrigin[0] + worldToScreen[3][1] * vOrigin[1] + worldToScreen[3][2] * vOrigin[2] + worldToScreen[3][3]; //Calculate the angle in compareson to the player's camera.
	vScreen.z = 0; //Screen doesn't have a 3rd dimension.

	//gBaseAPI.LogToFile("Checking");

	if( w > 0.001 ) //If the object is within view.
	{
		float fl1DBw = 1 / w; //Divide 1 by the angle.
		//gBaseAPI.LogToFile("Calculating X");
		vScreen.x = (gScreenSize.iScreenWidth / 2) + ( 0.5 * ((worldToScreen[0][0] * vOrigin[0] + worldToScreen[0][1] * vOrigin[1] + worldToScreen[0][2] * vOrigin[2] + worldToScreen[0][3]) * fl1DBw) * gScreenSize.iScreenWidth + 0.5); //Get the X dimension and push it in to the Vector.
		//gBaseAPI.LogToFile("Calculating Y");
		vScreen.y = (gScreenSize.iScreenHeight / 2) - ( 0.5 * ((worldToScreen[1][0] * vOrigin[0] + worldToScreen[1][1] * vOrigin[1] + worldToScreen[1][2] * vOrigin[2] + worldToScreen[1][3]) * fl1DBw) * gScreenSize.iScreenHeight + 0.5); //Get the Y dimension and push it in to the Vector.
		return true;
	}

	//gBaseAPI.LogToFile("Failed");
	return false;
}