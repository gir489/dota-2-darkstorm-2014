#include "SDK.h"
#include "VMT Hook.h"
#include "Client.h"
#include "Panels.h"

CInput* g_pInput;
CEntList* g_pEntList;
EngineClient* g_pEngine;
IGameResources* g_pPlayerResources;
IPanel* g_pIPanel;
ISurface* g_pSurface;
CHLClient* g_pClient;

COffsets gOffsets;
CPlayerVariables gPlayerVars;

CreateInterface_t EngineFactory = NULL;
CreateInterface_t ClientFactory = NULL;
CreateInterface_t VGUIFactory = NULL;
CreateInterface_t VGUI2Factory = NULL;

DWORD WINAPI dwMainThread( LPVOID lpArguments )
{
	if (g_pClient == NULL) //Prevent repeat callings.
	{
		//Gottammove those factorys up.
		//Grab the factorys from their resptive module's EAT.
		ClientFactory = ( CreateInterfaceFn ) GetProcAddress( gBaseAPI.GetModuleHandleSafe( "client.dll" ), "CreateInterface" );
		g_pClient = ( CHLClient* )ClientFactory( "VClient020", NULL);
		XASSERT(g_pClient);
		g_pEntList = ( CEntList* ) ClientFactory( "VClientEntityList003", NULL );
		EngineFactory = ( CreateInterfaceFn ) GetProcAddress( gBaseAPI.GetModuleHandleSafe( "engine.dll" ), "CreateInterface" );
		g_pEngine = ( EngineClient* ) EngineFactory( "VEngineClient018", NULL );
		VGUIFactory = ( CreateInterfaceFn ) GetProcAddress( gBaseAPI.GetModuleHandleSafe( "vguimatsurface.dll" ), "CreateInterface" );
		g_pSurface = ( ISurface* ) VGUIFactory( "VGUI_Surface031", NULL );
		VGUI2Factory = ( CreateInterfaceFn ) GetProcAddress( gBaseAPI.GetModuleHandleSafe( "vgui2.dll" ), "CreateInterface" );

		//Setup the CHLClient hooks.
		//if( g_pClient )
		//{
		//	CVMTHookManager* g_pClientHook = NULL;
		//	g_pClientHook = new CVMTHookManager( (PDWORD*)g_pClient ); //Does the same thing as CVMTHookManager::bInitialize.
		//	extern CreateMoveFn OriginalCreateMove;
		//	OriginalCreateMove = (CreateMoveFn)g_pClientHook->dwHookMethod( ( DWORD )Hooked_CreateMove, 21 ); //Hook CreateMove and store the address of the original in OriginalCreateMove. 
		//	g_pClientHook->dwHookMethod( ( DWORD )Hooked_WriteUserCmdDeltaToBuffer, 23 ); //This is required so it doesn't CRC the CUserCmd.  We're not going to call the original, because we're reconstructing the function.
		//	DWORD dwInputPointer = (gBaseAPI.dwFindPattern((DWORD)OriginalCreateMove, ((DWORD)OriginalCreateMove) + 0x100, "8B 0D")) + (0x2); //Find the pointer to CInput in CHLClient::CreateMove.
		//	g_pInput = **(CInput***)dwInputPointer;
		//}

		//Setup the Panel hook so we can draw.
		if( !g_pIPanel )
		{
			g_pIPanel = ( IPanel* ) VGUI2Factory( "VGUI_Panel009", NULL );
			XASSERT( g_pIPanel );

			if( g_pIPanel )
			{
				CVMTHookManager* g_pPanelHook = new CVMTHookManager; //Setup our CVMTManager for Panels.
				extern PaintTraverseFn OriginalPaintTraverse;
				extern void __stdcall Hooked_PaintTraverse(int unknown, unsigned int vguiPanel, bool forceRepaint, bool allowForce);
				if( g_pPanelHook->bInitialize( ( PDWORD* )g_pIPanel ) ) //If the VMTManager was able to hook the table.
				{
					OriginalPaintTraverse = (PaintTraverseFn)g_pPanelHook->dwHookMethod((DWORD)Hooked_PaintTraverse, 45);
				}
			}
		}
	}
	return 0; //The thread has been completed, and we do not need to call anything once we're done. The call to Hooked_PaintTraverse is now our main thread.
}

int WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	//If you manually map, make sure you setup this function properly.
	if(dwReason == DLL_PROCESS_ATTACH)
	{
		CreateThread( NULL, 0, (LPTHREAD_START_ROUTINE)dwMainThread, NULL, 0, NULL ); //CreateThread > _BeginThread. (For what we're doing, of course.)
	}
	return true;
}