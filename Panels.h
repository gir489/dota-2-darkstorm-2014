#pragma once

typedef struct CScreenSize_t
{
	int iScreenHeight;
	int iScreenWidth;

} CScreenSize;

typedef void* ( __stdcall* PaintTraverseFn )( int, int, bool, bool );
typedef void* ( __stdcall* AchievementMgrFn )( float );
void Intro( );
void fnCvarRenamer(const char* chOrigName, const char* chNewName, const char* dummyname );

extern CScreenSize gScreenSize;