#pragma once

#include <windows.h>
#include <math.h>
#include <xstring>
#include "Vector.h"
#include "getvfunc.h"
#include "dt_recv2.h"
#include "CBaseAPI.h"
#include "WeaponList.h"
#include "CGlobalVars.h"

using namespace std;

typedef void* ( __cdecl* CreateInterface_t )( const char*, int* );
typedef void* (*CreateInterfaceFn)(const char *pName, int *pReturnCode);

#define WIN32_LEAN_AND_MEAN
#pragma optimize("gsy",on)
#pragma warning( disable : 4244 ) //Possible loss of data

typedef float matrix3x4[3][4];

#define me g_pEngine->GetLocalPlayer()
#define GetBaseEntity g_pEntList->GetClientEntity
#define MASK_AIMBOT 0x200400B
#define	FL_ONGROUND (1<<0)
#define FL_DUCKING (1<<1)
#define CONTENTS_HITBOX 0x40000000
#define FLOW_OUTGOING 0
#define FLOW_INCOMING 1
#define PI 3.14159265358979323846f
#define DEG2RAD( x ) ( ( float )( x ) * ( float )( ( float )( PI ) / 180.0f ) )
#define RAD2DEG( x ) ( ( float )( x ) * ( float )( 180.0f / ( float )( PI ) ) )
#define square( x ) ( x * x )
#define RADPI 57.295779513082f
#define SQUARE( a ) a*a
#define BLU_TEAM 3
#define RED_TEAM 2

enum source_lifestates
{
	LIFE_ALIVE,
	LIFE_DYING,
	LIFE_DEAD,
	LIFE_RESPAWNABLE,
	LIFE_DISCARDBODY,
};

typedef struct player_info_s
{
	char			name[32];
	int				userID;
	char			guid[33];
	unsigned long	friendsID;
	char			friendsName[32];
	bool			fakeplayer;
	bool			ishltv;
	unsigned long	customFiles[4];
	unsigned char	filesDownloaded;
} player_info_t;

class ClientClass
{
private:
	BYTE _chPadding[8];
public:
	char* chName;
	RecvTable* Table;
	ClientClass* pNextClass;
	int iClassID;
};

class CHLClient
{
public:
	ClientClass* GetAllClasses( void )
	{
		typedef ClientClass* ( __thiscall* OriginalFn )( PVOID ); //Anything inside a VTable is a __thiscall unless it completly disregards the thisptr. You can also call them as __stdcalls, but you won't have access to the __thisptr.
		return getvfunc<OriginalFn>( this, 7 )( this ); //Return the pointer to the head CClientClass.
	}
};

class CGlobals
{
public:
	float realtime;
	int framecount;
	float absoluteframetime;
	float curtime;
	float frametime;
	int maxclients;
	int tickcount;
	float interval_per_tick;
	float interpolation_amount;
};

class CUserCmd
{
public:
	virtual ~CUserCmd() {}; //Destructor 0
	int command_number; //4
	int tick_count; //8
	Vector viewangles; //C
	float forwardmove; //18
	float sidemove; //1C
	float upmove; //20
	int	buttons; //24
	byte impulse; //28
	int weaponselect; //2C
	int weaponsubtype; //30
	int random_seed; //34
	short mousedx; //38
	short mousedy; //3A
	bool hasbeenpredicted; //3C;
};

class IConVar
{
public:
	void SetActualValue( const char* pszValue )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, const char* );
		return getvfunc<OriginalFn>( this, 2 )( this, pszValue );
	}
	void SetValue( const char* pszValue )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, const char* );
		PVOID pBase = (PVOID)( this + 0x18 );
		return getvfunc<OriginalFn>( pBase, 2 )( pBase, pszValue );
	}
	float GetFloat ( void )
	{
		DWORD dwBase = *((DWORD*)( this + 0x1C));
		return *(float*)( dwBase + 0x2C );
	}
};

class ICvar
{
public:
	IConVar* FindVar( const char* chName )
	{
		typedef IConVar* ( __thiscall* OriginalFn )( PVOID, const char* );
		return getvfunc<OriginalFn>( this, 13 )( this, chName );
	}
	DWORD* FindCommand( const char* chName )
	{
		typedef DWORD* ( __thiscall* OriginalFn )( PVOID, const char* );
		return getvfunc<OriginalFn>( this, 15 )( this, chName );
	}
};

class IGameResources
{
public:
	virtual ~IGameResources() {};

	virtual const char* GetTeamName(int iTeamIndex) = 0;
	virtual int GetTeamScore(int iTeamIndex) = 0;
	virtual const DWORD& GetTeamColor(int iTeamIndex) = 0;
	virtual bool IsConnected(int iEntityIndex) = 0;
	virtual bool IsAlive(int iEntityIndex) = 0;
	virtual bool IsFakePlayer(int iEntityIndex) = 0;
	virtual bool IsLocalPlayer(int iEntityIndex) = 0;
	virtual const char* GetPlayerName(int iEntityIndex) = 0;
	virtual int GetPlayerScore(int iEntityIndex) = 0;
	virtual int GetPing(int iEntityIndex) = 0;
	virtual int GetDeaths(int iEntityIndex) = 0;
	virtual int GetFrags(int iEntityIndex) = 0;
	virtual int GetTeam(int iEntityIndex) = 0;
	virtual int GetHealth(int iEntityIndex) = 0;
};

class CBaseEntity;
class CEntList;

class CEntList
{
public:
	CBaseEntity* GetClientEntity(int entnum)
	{
		typedef CBaseEntity* (__thiscall* OriginalFn)(PVOID, int);
		return getvfunc<OriginalFn>(this, 3)(this, entnum);
	}
	CBaseEntity* GetClientEntityFromHandle(int hEnt)
	{
		typedef CBaseEntity* (__thiscall* OriginalFn)(PVOID, int);
		return getvfunc<OriginalFn>(this, 4)(this, hEnt);
	}
	int GetHighestEntityIndex(void)
	{
		typedef int(__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 6)(this);
	}
};

extern CEntList* g_pEntList;

class CBaseEntity
{
public:
	int GetHealth()
	{
		return *(int*)((DWORD)(this) + 0xF4);
	}
	float GetMana()
	{
		return *(float*)((DWORD)(this) + 0x1148);
	} 
	bool IsIllusion()
	{
		CBaseEntity* pBaseEnt = g_pEntList->GetClientEntityFromHandle(*(int*)((DWORD)(this) + 0x36AC));
		return (pBaseEnt != NULL);
	}
	bool IsAlive()
	{
		return (*(PBYTE)((DWORD)this + 0x253) == LIFE_ALIVE);
	}
	const char* GetName()
	{
		typedef const char* (__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 304)(this);
	}
	Vector& GetAbsOrigin()
	{
		typedef Vector& (__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 11)(this);
	}
	Vector& GetAbsAngles()
	{
		typedef Vector& (__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 12)(this);
	}
	void GetWorldSpaceCenter( Vector& vWorldSpaceCenter)
	{
		Vector vMin, vMax;
		this->GetRenderBounds( vMin, vMax );
		vWorldSpaceCenter = this->GetAbsOrigin();
		vWorldSpaceCenter.z += (vMin.z + vMax.z) / 2;
	}
	DWORD* GetModel( )
	{
		PVOID pRenderable = (PVOID)(this + 0x4);
		typedef DWORD* ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( pRenderable, 8 )( pRenderable );
	}
	bool SetupBones( matrix3x4 *pBoneToWorldOut, int nMaxBones, int boneMask, float currentTime )
	{
		PVOID pRenderable = (PVOID)(this + 0x4);
		typedef bool ( __thiscall* OriginalFn )( PVOID, matrix3x4*, int, int, float );
		return getvfunc<OriginalFn>(pRenderable, 13)(pRenderable, pBoneToWorldOut, nMaxBones, boneMask, currentTime);
	}
	ClientClass* GetClientClass( )
	{
		PVOID pNetworkable = (PVOID)(this + 0x8);
		typedef ClientClass* ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( pNetworkable, 2 )( pNetworkable );
	}
	bool IsDormant( )
	{
		PVOID pNetworkable = (PVOID)(this + 0x8);
		typedef bool ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>(pNetworkable, 9)(pNetworkable);
	}
	int GetIndex( )
	{
		PVOID pNetworkable = (PVOID)(this + 0x8);
		typedef int ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( pNetworkable, 10 )( pNetworkable );
	}
	void GetRenderBounds( Vector& mins, Vector& maxs )
	{
		PVOID pRenderable = (PVOID)(this + 0x4);
		typedef void ( __thiscall* OriginalFn )( PVOID, Vector& , Vector& );
		getvfunc<OriginalFn>(pRenderable, 17)(pRenderable, mins, maxs);
	}
};

class EngineClient
{
public:
	void GetScreenSize( int& width, int& height )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, int& , int& );
		return getvfunc<OriginalFn>( this, 5 )( this, width, height );
	}
	bool GetPlayerInfo( int ent_num, player_info_t *pinfo )
	{
		typedef bool ( __thiscall* OriginalFn )( PVOID, int, player_info_t * );
		return getvfunc<OriginalFn>(this, 8)(this, ent_num, pinfo );
	}
	bool Con_IsVisible( void )
	{
		typedef bool ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 11 )( this );
	}
	int GetLocalPlayer( void )
	{
		typedef int ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 12 )( this );
	}
	float Time( void )
	{
		typedef float ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 14 )( this );
	}
	void GetViewAngles( Vector& va )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, Vector& va );
		return getvfunc<OriginalFn>( this, 19 )( this, va );
	}
	void SetViewAngles( Vector& va )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, Vector& va );
		return getvfunc<OriginalFn>( this, 20 )( this, va );
	}
	int GetMaxClients( void )
	{
		typedef int ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 21 )( this );
	}
	bool IsInGame( void )
	{
		typedef bool ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 26 )( this );
	}
	bool IsConnected( void )
	{
		typedef bool ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 27 )( this );
	}
	bool IsDrawingLoadingImage( void )
	{
		typedef bool ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>( this, 28 )( this );
	}
	const matrix3x4& WorldToScreenMatrix( void )
	{
		typedef const matrix3x4& ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>(this, 37)(this);
	}
	bool IsTakingScreenshot( void )
	{
		typedef bool ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>(this, 91)(this);
	}
	DWORD* GetNetChannelInfo( void )
	{
		typedef DWORD* ( __thiscall* OriginalFn )( PVOID );
		return getvfunc<OriginalFn>(this, 75)(this);
	}
	void ClientCmd_Unrestricted( const char* chCommandString )
	{
		typedef void ( __thiscall* OriginalFn )( PVOID, const char * );
		return getvfunc<OriginalFn>(this, 109)(this, chCommandString);
	}
};

class IPanel
{
public:
	const char *GetName(unsigned int vguiPanel)
	{
		typedef const char* ( __thiscall* OriginalFn )( PVOID, unsigned int );
		return getvfunc<OriginalFn>(this, 40)(this, vguiPanel);
	}
};

class ISurface
{
public:
	void DrawSetColor(int r, int g, int b, int a)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int, int, int);
		getvfunc<OriginalFn>(this + 0x10, 17)(this + 0x10, r, g, b, a);
	}
	void DrawFilledRect(int x0, int y0, int x1, int y1)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int, int, int);
		getvfunc<OriginalFn>(this + 0x10, 21)(this + 0x10, x0, y0, x1, y1);
	}
	void DrawOutlinedRect(int x0, int y0, int x1, int y1)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int, int, int);
		getvfunc<OriginalFn>(this + 0x10, 19)(this + 0x10, x0, y0, x1, y1);
	}
	void DrawSetTextFont(unsigned long font)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, unsigned long);
		getvfunc<OriginalFn>(this + 0x10, 13)(this + 0x10, font);
	}
	void DrawSetTextColor(int r, int g, int b, int a)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int, int, int);
		getvfunc<OriginalFn>(this + 0x10, 26)(this + 0x10, r, g, b, a);
	}
	void DrawSetTextPos(int x, int y)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int);
		getvfunc<OriginalFn>(this + 0x10, 27)(this + 0x10, x, y);
	}
	void DrawPrintText(const wchar_t *text, int textLen)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, const wchar_t *, int, int);
		return getvfunc<OriginalFn>(this + 0x10, 28)(this + 0x10, text, textLen, 0);
	}
	unsigned long CreateFont()
	{
		typedef unsigned int(__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 52)(this);
	}
	void SetFontGlyphSet(unsigned long &font, const char *windowsFontName, int tall, int weight, int blur, int scanlines, int flags)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, unsigned long, const char*, int, int, int, int, int, int, int);
		getvfunc<OriginalFn>(this, 53)(this, font, windowsFontName, tall, weight, blur, scanlines, flags, 0, 0);
	}
	void GetTextSize(unsigned long font, const wchar_t *text, int &wide, int &tall)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, unsigned long, const wchar_t *, int&, int&);
		getvfunc<OriginalFn>(this, 60)(this, font, text, wide, tall);
	}
};

enum playercontrols
{
	IN_ATTACK = (1 << 0),
	IN_JUMP	= (1 << 1),
	IN_DUCK = (1 << 2),
	IN_FORWARD = (1 << 3),
	IN_BACK = (1 << 4),
	IN_USE = (1 << 5),
	IN_CANCEL = (1 << 6),
	IN_LEFT = (1 << 7),
	IN_RIGHT = (1 << 8),
	IN_MOVELEFT = (1 << 9),
	IN_MOVERIGHT = (1 << 10),
	IN_ATTACK2 = (1 << 11),
	IN_RUN = (1 << 12),
	IN_RELOAD = (1 << 13),
	IN_ALT1 = (1 << 14),
	IN_ALT2 = (1 << 15),
	IN_SCORE = (1 << 16),	// Used by client.dll for when scoreboard is held down
	IN_SPEED = (1 << 17),	// Player is holding the speed key
	IN_WALK = (1 << 18),	// Player holding walk key
	IN_ZOOM	= (1 << 19),	// Zoom key for HUD zoom
	IN_WEAPON1 = (1 << 20),	// weapon defines these bits
	IN_WEAPON2 = (1 << 21),	// weapon defines these bits
	IN_BULLRUSH = (1 << 22),
};

enum TFCond
{
    TFCond_Slowed = (1 << 0), //Toggled when a player is slower than normal.
    TFCond_Zoomed = (1 << 1), //Toggled when a player is zoomed in.
    TFCond_Disguising = (1 << 2), //Toggled when a Spy is disguising. 
    TFCond_Disguised = (1 << 3), //Toggled when a Spy is disguised.
    TFCond_Cloaked = (1 << 4), //Toggled when a Spy is invisible.
    TFCond_Ubercharged = (1 << 5), //Toggled when a player is ‹berCharged.
    TFCond_TeleportedGlow = (1 << 6), //Will activate when someone leaves a teleporter and has glow beneath their feet.
    TFCond_Taunting = (1 << 7), //Activates when a player is taunting.
    TFCond_UberchargeFading = (1 << 8), //Activates when the ‹berCharge is fading.
    TFCond_CloakFlicker = (1 << 9), //When a normal cloak Spy gets bumped into, or a CloakAndDagger spy with no energy is moving.
    TFCond_Teleporting = (1 << 10), //Only activates for a brief second when a player is riding a teleporter; not very useful.
    TFCond_Kritzkrieged = (1 << 11), //When a player has a crit buff from the KrRitzkrieg. (No longer used?)
    TFCond_TmpDamageBonus = (1 << 12), //Unknown what this is for.
    TFCond_DeadRingered = (1 << 13), //Toggled when the player is under reduced damage from the Deadringer.
    TFCond_Bonked = (1 << 14), //Player is under the effects of Bonk! Atomic Punch.
    TFCond_Stunned = (1 << 15), //Player was stunned from a Sandman ball.
    TFCond_Buffed = (1 << 16), //Toggled when a player is within a Buff Banner's range.
    TFCond_Charging = (1 << 17), //Toggled when a Demo Knight charges with the shield.
    TFCond_DemoBuff = (1 << 18), //Toggled when a Demo Knight has heads from the Eyelander.
    TFCond_CritCola = (1 << 19), //Toggled when the player is under the effect of Crit-a-Cola.
    TFCond_InHealRadius = (1 << 20), //Unknown what this is for.
    TFCond_Healing = (1 << 21), //Toggled when someone is being healed by a medic or a dispenser.
    TFCond_OnFire = (1 << 22), //Toggled when a player is on fire.
    TFCond_Overhealed = (1 << 23), //Toggled when a player has >100% health.
    TFCond_Jarated = (1 << 24), //Toggled when a player is hit with a sniper's Jarate.
    TFCond_Bleeding = (1 << 25), //Toggled from Boston Basher/Tribalman's Shiv/Southern Hospitality damage.
    TFCond_DefenseBuffed = (1 << 26), //Toggled when a player is within a Battalion's Backup's range.
    TFCond_Milked = (1 << 27), //Player was hit with a jar of Mad Milk.
    TFCond_MegaHeal = (1 << 28), //Player is under the effect of Quick-Fix charge.
    TFCond_RegenBuffed = (1 << 29), //Toggled when a player is within a Concheror's range.
    TFCond_MarkedForDeath = (1 << 30), //Player is marked for death by a Fan O'War hit. Effects are similar to TFCond_Jarated.

    TFCondEx_SpeedBuffAlly = (1 << 0), //Toggled when a player gets hit with the disciplinary action.
#ifdef HALLOWEEN
    TFCondEx_HalloweenCritCandy = (1 << 1), //Only for Scream Fortress event maps that drop crit candy.
#endif
    TFCondEx_CritHype = (1 << 4), //Soda Popper crits.
    TFCondEx_CritOnFirstBlood = (1 << 5), //Arena first blood crit buff.
    TFCondEx_CritOnWin = (1 << 6), //End of round crits.
    TFCondEx_CritOnFlagCapture = (1 << 7), //CTF intelligence capture crits.
    TFCondEx_CritOnKill = (1 << 8), //Unknown what this is for.
    TFCondEx_RestrictToMelee = (1 << 9), //Unknown what this is for.
    TFCondEx_PyroCrits = (1 << 12), //Pyro is getting crits from the Mmmph charge.
    TFCondEx_PyroHeal = (1 << 13), //Pyro is being healed from the Mmmph charge and can not be damaged.

    TFCond_Crits = ( TFCond_Kritzkrieged ),
    TFCond_MiniCrits = ( TFCond_Buffed | TFCond_CritCola ),
    TFCondEx_Crits = (
#ifdef HALLOWEEN
                        TFCondEx_HalloweenCritCandy |
#endif
                        TFCondEx_CritOnFirstBlood | TFCondEx_CritOnWin | TFCondEx_CritOnFlagCapture | TFCondEx_CritOnKill | TFCondEx_PyroCrits ),
    TFCondEx_MiniCrits = ( TFCondEx_CritHype ),
	TFCond_IgnoreStates = ( TFCond_Ubercharged | TFCond_Bonked ),
	TFCondEx_IgnoreStates = ( TFCondEx_PyroHeal )
};

enum tf_classes
{
	TF2_Scout = 1,
	TF2_Soldier = 3,
	TF2_Pyro = 7,
	TF2_Demoman = 4,
	TF2_Heavy = 6,
	TF2_Engineer = 9,
	TF2_Medic = 5,
	TF2_Sniper = 2,
	TF2_Spy = 8,
};

class CInput
{
public:
	CUserCmd* GetUserCmd( int seq )
	{
		typedef CUserCmd* ( __thiscall* OriginalFn )( PVOID, int );
		return getvfunc<OriginalFn>( this, 8 )( this, seq );
	}
};

#define XASSERT( x ) if( !x ) MessageBoxW( 0, L#x, 0, 0 );

extern CInput* g_pInput;
extern CHLClient* g_pClient;
extern EngineClient* g_pEngine;
extern IPanel* g_pIPanel;
extern ISurface* g_pSurface;
extern DWORD* g_pMoveHelper;
extern ICvar* g_pCvar;

extern CPlayerVariables gPlayerVars;
extern COffsets gOffsets;